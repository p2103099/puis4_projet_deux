package interfaceJeuUn;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import puisQuatre.Puis4;



public class SourisListener extends MouseAdapter 
implements MouseListener, Runnable {
    private Puis4 p4;
    private int c;

    public SourisListener(Puis4 p4, int c) {
        this.p4=p4;
        this.c=c;
    }

    public void mousePressed(MouseEvent e){
        if (!p4.estTermine() && p4.estPossibleJouer(c)){
            p4.joueurJoue(c);
            Thread t = new Thread(this);
            t.start();			
        }
    }

    public void run() {		
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        p4.progJoue();
    }
}
