package interfaceJeuUn;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import puisQuatre.Puis4;

public class P4Frame extends JFrame  {
    private static final long serialVersionUID = 8998394289259804336L;
    public static final int LARGUEUR = 400;
    public static final int HAUTEUR = 500;	

    public P4Frame(Puis4 p4){
        this.setSize(LARGUEUR, HAUTEUR);
        this.setTitle("Puissance 4");
        this.setLayout(new BorderLayout()); 
        P4Panel p = new P4Panel(p4);
        this.add(p,BorderLayout.CENTER);	
        this.addWindowListener(new WindowCloser(this));
    }

    public P4Frame() {
        this(new Puis4());
    }

    public static void main(String[] args) {
        JFrame  frame = new P4Frame();
        frame.setVisible(true);
    }
}
