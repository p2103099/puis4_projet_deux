package interfaceJeuUn;

import java.awt.GridLayout;

import javax.swing.JPanel;

import puisQuatre.Puis4;
public class JeuPanel extends JPanel {
    private static final long serialVersionUID = 6392270319142613576L;

    public JeuPanel(Puis4 p4) {
        this.setLayout(new GridLayout(p4.getNbLignes(),p4.getNbColonnes()));
        for (int l=p4.getNbLignes()-1; l>=0;l--){
            for (int c=0; c<p4.getNbColonnes();c++){			
                Cellule cel = new Cellule(c,l,p4);
                this.add(cel);
            }
        }
    }
}
