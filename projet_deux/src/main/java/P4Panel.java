package interfaceJeuUn;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import puisQuatre.Puis4;

public class P4Panel extends JPanel implements PropertyChangeListener {
    private static final long serialVersionUID = -8496744297660506385L;
    private Puis4 p4;
    private JTextField tdEtat;

    public P4Panel(Puis4 p4){
        this.p4=p4;
        p4.addPropertyChangeListener(this);
        this.setLayout(new BorderLayout());
        JPanel pNord = new JPanel();
        pNord.add(new JLabel("Etat du jeu"));
        tdEtat = new JTextField("Nouveau Jeu", 10);
        tdEtat.setEditable(false);
        pNord.add(tdEtat);
        this.add(pNord,BorderLayout.NORTH);
        JeuPanel pCentre = new JeuPanel(p4);
        this.add(pCentre,BorderLayout.CENTER);	
        JPanel pSud = new JPanel();
        JButton nouveau = new JButton("nouveau");
        pSud.add(nouveau);
        nouveau.addActionListener(new NouveauListener(p4));
        this.add(pSud,"South");
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (!p4.estTermine())
            tdEtat.setText("A vous de jouer");
        else if (p4.estProgGagne())
            tdEtat.setText("Vous avez perdu");
        else if (p4.estJoueurGagne())
            tdEtat.setText("Vous avez gagné");      
    }
}
