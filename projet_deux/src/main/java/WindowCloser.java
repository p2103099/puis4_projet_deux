package interfaceJeuUn;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowCloser extends WindowAdapter {
    private Frame f;

    public WindowCloser(Frame f) {
        this.f = f;
    }

    @Override
    public void windowClosing(WindowEvent e){
        f.dispose();
    }
}
