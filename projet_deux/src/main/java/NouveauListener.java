package interfaceJeuUn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import puisQuatre.Puis4;
public class NouveauListener implements ActionListener {
    private Puis4 p4;

    public NouveauListener(Puis4 p4) {
        this.p4 = p4;
    }

    public void actionPerformed(ActionEvent e) {
        p4.nouveauJeu();

    }
}
