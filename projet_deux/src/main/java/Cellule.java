package interfaceJeuUn;

import java.awt.Color;
import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;

import puisQuatre.Puis4;

public class Cellule extends JPanel implements PropertyChangeListener{
    private static final long serialVersionUID = 130062958599264217L;
    private int c,  l;
    private Puis4 p4;

    public Cellule(int c, int l, Puis4 p4) {
        this.c = c;
        this.l = l;
        this.p4 = p4;
        p4.addPropertyChangeListener(this);
        this.addMouseListener(new SourisListener(p4,c));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int taille = Math.min(this.getHeight(),this.getWidth());
        g.drawRect(0,0,this.getWidth()-1,this.getHeight()-1);
        if (p4.getPion(c,l)==Puis4.JOUEUR){
            g.setColor(Color.BLUE);
            g.fillOval(0,0,taille,taille);
        }
        if (p4.getPion(c,l)==Puis4.PROG){	
            g.setColor(Color.RED);
            g.fillOval(0,0,taille,taille);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.repaint();      
    }
}
